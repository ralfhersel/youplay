#!/usr/bin/env bash
# Project:        youplay
# File:           scripts/Make_AppImage.sh
# Author:         Prof.P
# Licence:        GPL3
#
# Make AppImage from Python program with Python-AppImage
# ARCH string can be "x86_64" or "i686"
# -----------------------------------------------------------------------
VERSION="0.46"
ARCH="x86_64"
APPIMAGENAME="youplay_v$VERSION-Qt6-$ARCH.AppImage"
StartDir=$PWD
BaseDir=$(dirname "$StartDir")
AppDir=$BaseDir/AppDir
DistDir=$BaseDir/dist
TmpDir=$DistDir/_tmp
LogFile=$BaseDir/dist/Make_AppImage.log
mkdir -p $DistDir && mkdir -p $TmpDir
# -----------------------------------------------------------------------
echo -e "Make AppImage from Python code by help of Python AppImage\n" |tee $LogFile
read -p "Continue with ENTER . . ."
echo -e "-----------------------------------------------------------------------" |tee -a $LogFile

# -----------------------------------------------------------------------
# remove previous build files (suppress not-existing error)
rm -rf $AppDir &>/dev/null

# -----------------------------------------------------------------------
# download the necessary tools.
echo -e "[1/9] - Downloading the necessary tools." |tee -a $LogFile
wget https://github.com/AppImage/AppImageKit/releases/download/continuous/appimagetool-x86_64.AppImage -O $TmpDir/appimagetool &>> $LogFile
wget https://github.com/niess/python-appimage/releases/download/python3.11/python3.11.4-cp311-cp311-manylinux_2_28_$ARCH.AppImage -O $TmpDir/pythonappimage &>> $LogFile
# make them executable so that we can call them (and also, plugins called from linuxdeploy are called like binaries)
chmod a+x $TmpDir/appimagetool $TmpDir/pythonappimage

cd $BaseDir

# Create AppImage from Python AppImage
echo -e "[2/9] - Creating AppImage from Python AppImage." |tee -a $LogFile
$TmpDir/pythonappimage --appimage-extract &>/dev/null |tee -a $LogFile
mv squashfs-root $AppDir

# install necessary pip packages to an alternate location
echo -e "[3/9] - Installing necessary pip packages to an alternate location." |tee -a $LogFile
mkdir $AppDir/packages
$AppDir/opt/python3.11/bin/python3.11 -m pip install -r requirements_qt.txt --target=$AppDir/packages |& tee -a $LogFile &>/dev/null

# clean up AppImage
echo -e "[4/9] - Cleaning up AppImage." |tee -a $LogFile
rm -f $./pythonappimage
rm $AppDir/python3*desktop
rm $AppDir/python.png
rm $AppDir/AppRun

# include our app in the extracted Python AppImage
echo -e "[5/9] - Including our app in the extracted Python AppImage." |tee -a $LogFile
cp youplay.py $AppDir/
cp youplay_qt6.py $AppDir/
cp youplay.svg $AppDir/
cp youplay.desktop $AppDir/
mkdir $AppDir/img && cp img/icons8-search-50.png $AppDir/img/icons8-search-50.png

# ...REPLACING THE EXISTING APPRUN WITH A CUSTOM ONE...
cat >> $AppDir/AppRun << 'EOF'
#!/usr/bin/env bash

# If running from an extracted image, then export ARGV0 and APPDIR
if [ -z "${APPIMAGE}" ]; then
    export ARGV0="$0"

    self=$(readlink -f -- "$0") # Protect spaces (issue 55)
    here="${self%/*}"
    export APPDIR=$here
fi

# Export TCl/Tk
export TCL_LIBRARY="${APPDIR}/usr/share/tcltk/tcl8.6"
export TK_LIBRARY="${APPDIR}/usr/share/tcltk/tk8.6"
export TKPATH="${TK_LIBRARY}"

# Export SSL certificate
export SSL_CERT_FILE="${APPDIR}/opt/_internal/certs.pem"

# Resolve the calling command (preserving symbolic links).
export APPIMAGE_COMMAND=$(command -v -- "$ARGV0")

# Prefix PATH env. with packages directory
export PYTHONPATH="${APPDIR}/packages"
# export LD_LIBRARY_PATH="${APPDIR}/usr/lib:$LD_LIBRARY_PATH"

cd $APPDIR

if [ -z "$@" ]
then # if no command line argument was passed, assume GUI mode
      "$APPDIR/opt/python3.11/bin/python3.11" "youplay.py" "--gui=qt"
else # launch youplay with command line args
      "$APPDIR/opt/python3.11/bin/python3.11" "youplay.py" "$@"
fi
EOF
chmod a+x $AppDir/AppRun

# replace icon in desktop-file (necessary for AppImage compliance)
sed -i 's/^Icon=.*/Icon=youplay/' $AppDir/youplay.desktop

# remove some great files because AppImage gets too large
rm $AppDir/packages/PySide6/lupdate
rm $AppDir/packages/PySide6/QtOpenGL.abi3.so
find $AppDir/packages/PySide6/ -type f -name '*libQt6Quick*' -delete

# repackage the newly created AppImage
echo -e "[6/9] - Repackaging the newly created AppImage." |tee -a $LogFile
ARCH=$ARCH $TmpDir/appimagetool $AppDir $APPIMAGENAME |tee -a $LogFile

# remove temp files
echo -e "[7/9] - Removing temporary files." |tee -a $LogFile
rm -rf $TmpDir

# move appimage to dist dir
echo -e "[8/9] - Moving AppImage to dir $DistDir." |tee -a $LogFile
mkdir -p $DistDir
mv $APPIMAGENAME $DistDir

# go back to where we started from
cd $StartDir

# display end message
echo -e "[9/9] - Done creating $DistDir/$APPIMAGENAME." |tee -a $LogFile
echo -e "=======================================================================" |tee -a $LogFile
echo -e "(This window can be closed.)"

# make notification sound
if command -v 'paplay' &>/dev/null; then
  if test -f "/usr/share/sounds/freedesktop/stereo/complete.oga"; then
    paplay "/usr/share/sounds/freedesktop/stereo/complete.oga"
  fi
fi
