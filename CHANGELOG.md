YouPlay
=======================
# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.46] - 2023-05-30

### Changed

- GTK4-module: button margins added (left of Play, right of Quit)
- QT-module: moved "About Qt" button into About-Dialog
- string formating replaced by f-strings

## [0.45] - 2023-05-25

### Added

- Qt-port (based on PySide6 python bindings for Qt GUI-Toolkit)
- New [CHANGELOG.md](https://codeberg.org/ralfhersel/youplay/src/branch/main/CHANGELOG.md) file

### Security

- Code injection prevented

### Changed

- Minimum song title lenght reduced to 3 characters
- AUR package revived
- no functional changes

## [0.44] - 2023-04-27

### Added

- AppImage available (thank you, Prof. P.)

### Changed

- Updated installation instructions
- Flatpak suspended because GitHub account enforced
- no functional changes

## [0.43] - 2023-02-21

### Added

- Further preparations for the flatpak

### Fixed

### Changed

- Updated dependencies: yt-dlp, python-mpv

## [0.42] - 2023-01-12

### Added

- Further preparations for the flatpak

### Changed

- Another optimization of song title lenghts both in CLI und GUI

## [0.41] - 2023-01-07

### Added

- First steps to create a flatpak
- Known Issues:
    - Flatpak builds local, but not yet on Flathub
    - Table header alignment requires refactoring because of Gtk4
    - App-Icon still not shown

### Changed

- Optimization of song title lenghts both in CLI und GUI
- Right alignment of song duration
- Optimization of window width

### Removed

## [0.40] - 2022-12-19

- This is my birthday version :)

### Added

### Fixed

### Changed

- Migrated to Gtk4, LibAdwaita
- Known Issue:
- * Issues:
    - still no Flatplak
    - App icon does not work
    - probably regressions caused by the Gtk4 migration

## [0.38] - 2022-07-07

### Added

- Search icon in search entry
- Spacing and homogeneous distribution to the buttons

### Changed

- Window height to show all 10 song lines
- Status bar shows 'song title  -  play time' when playing song

### Removed

- Default text in search entry: 'Enter song name'

## [0.37] - 2022-05-31

### Changed

- Force `yt-dlp` to extract best audio in mp3 format. Last version had issues with forcing mp3 format.

## [0.36] - 2022-05-10

### Changed

- No more webm in between. `yt-dlp` will download mp3 directly.

## [0.35] - yyyy-mm-dd

### Fixed

- Special characters (/) in song title caused creation of subdirectories in `/music/youplay/` instead of just saving the song.

## [0.34] - yyyy-mm-dd

### Added

- Tried GTK4; was not successful. Will try again later.

### Changed

- Updating `yt-dlp` will fix the warnings in CLI-mode. You have to run `python3 -m pip install -U yt-dlp` to fight Google's changes in the Youtube API.
- Some warning text changes; not important.

### Removed

- Already out-commented old code; not important.

## [0.33] - 2021-12-08

### Changed

- Because `youtube-dl` is so much restricted in speed, I switched to `yt-dlp` instead.
- Currently `yt-dlp` throws a warning in CLI-mode. Ignore it, I will fix it later.

## [0.32] - 2021-05-26

### Changed

- GUI window is now centred on the screen.

## [0.31] - 2021-05-25

### Added

- Additional audio controls in GUI-Mode: Pause/Continue, Back, Forward. The Play/Stop and Pause/Continue controls are kind of overlapping in functionality, but who cares.
- Pause/Continue will either pause the song at the current position, or continue playing at the position where you paused it.
- Back/Forward will step 5 seconds back or forward during playback.
- Play/Stop will either start a selected song or stop it. If you press Play again, the song will start from the beginning.

## [0.30] - 2021-05-18

### Changed

- Until now, all songs were download as webm-files (but named as mp3 files) because youtube-dl ignores the mp3-format parameter. Thats why YouPlay converts all downloaded songs now from webm to mp3.

### Removed

- I removed the filename sanitizer because I think it is not necessary. We will see if it causes issues with weird song names.

## [0.29] - 2021-03-22

### Changed

- Improvement of process handling to avoid busy wait on process exit (thank you, Jürgen Hötzel :)


## [0.28] - 2021-01-20

### Fixed
- Minor bug fixes

### Changed

- Downloaded songs will go to 'youplay' subdirectory under your standard music folder. You can delete or copy the old music subfolder (thank you Joël)
- Search strings must be longer than 3 characters to avoid bullshit searches

### Removed

- File 'audio.list' not longer required because of direct search result processing. You can delete the old audio.list (thank you, j.r :)

## [0.27] - 2021-01-10

### Added
- Song names are sanitized to avoid invalid search strings in youtube-dl (welle erdball c64)

### Fixed

- Duplicated download of existing files fixed again (song.mp3.mp3.mp3)

## [0.26] - 2021-01-08

### Changed

- Heavy refactoring on the CLI control loop, run ''./youplay.py --help' for more infos
- CLI mode now shows already downloaded songs, run: './'youplay.py --songs'
- Further list alignment for song and search list, both in CLI- and GUI-mode.
- Installation scripts and packages are 'work in progress' and not yet ready.

### Removed

## [0.25] - 2021-01-04


### Changed

- Code clean-up

### Removed

- Mutagen dependency removed (song duration is now fetched with 'ffmpeg')

## [0.24] - 2021-01-03

### Added

### Fixed

- Progress bar works when a second song is searched

### Changed

- GUI song number centered
- Only one button to PLAY or STOP a song
- Doubleclick on a song while another one is playing, will stop the playing song and play the doubleclicked song
- The SONG-button will show and let you play all downloaded songs from the 'music' folder

### Removed

## [0.23] - 2020-12-23

### Added

- GUI mode shows playing time

### Fixed

- Columns and song titles are correctly adjusted to fit into window

### Changed

### Removed

## [0.22] - 2020-12-21

### Added

### Fixed

### Changed

- GUI mode will utilize the internal python-mpv player instead of the external mpv player
- PLAY-button will be disabled if you doubleclick song title or click the PLAY-button

### Removed

## [0.21] - 2020.12.19

(my birthday version)

### Added

### Fixed

### Changed

- Downloaded songs will be stored in the 'music' subfolder
- Already downloaded songs will not be downloaded again, but played immediately
- PLAY button disabled when song is playing (only when started from PLAY button, not from doubleclick)

### Removed

- Option to save a song removed from CLI and GUI, because it is obsolete
- ffmpeg dependency removed, because youtube-dl will download mp3 directly

## [0.20] - 2020-12-18

### Added

- Info about song duration added to CLI and GUI

### Fixed

### Changed

- Endless streams are omitted (by duration = 0)
- Number of songs reduced to 10 (to avoid lenghty loading time)

### Removed

## [0.19] - 2020-12-16

### Added

- Progress bar while searching, downloading and saving songs
- Progress bar for CLI and GUI mode

### Fixed

### Changed

### Removed

## [0.18] - 2020.12.15

### Added

- CLI-mode utilises MPV in CLI-mode
- GUI-mode starts MPV in GUI-mode
- Doubleclick in song list, starts download and playback of selected song

### Fixed

### Changed

- youplay.desktop-file doesn't require bash-script, but call youplay.py directly

[0.46]: https://codeberg.org/ralfhersel/youplay/compare/v0.45...v0.46
[0.45]: https://codeberg.org/ralfhersel/youplay/compare/v0.44...v0.45
[0.44]: https://codeberg.org/ralfhersel/youplay/compare/v0.43...v0.44
[0.43]: https://codeberg.org/ralfhersel/youplay/compare/v0.42...v0.43
[0.42]: https://codeberg.org/ralfhersel/youplay/compare/v0.41...v0.42
[0.41]: https://codeberg.org/ralfhersel/youplay/compare/v0.40...v0.41
[0.40]: https://codeberg.org/ralfhersel/youplay/compare/v0.39...v0.40
[0.38]: https://codeberg.org/ralfhersel/youplay/compare/v0.37...v0.38
[0.37]: https://codeberg.org/ralfhersel/youplay/compare/v0.36...v0.37
[0.36]: https://codeberg.org/ralfhersel/youplay/compare/v0.35...v0.36
[0.35]: https://codeberg.org/ralfhersel/youplay/compare/v0.34...v0.35
[0.34]: https://codeberg.org/ralfhersel/youplay/compare/v0.33...v0.34
[0.31]: https://codeberg.org/ralfhersel/youplay/compare/v0.30...v0.31
[0.30]: https://codeberg.org/ralfhersel/youplay/compare/v0.29...v0.30
[0.28]: https://codeberg.org/ralfhersel/youplay/compare/v0.28...v0.29
[0.28]: https://codeberg.org/ralfhersel/youplay/compare/v0.27...v0.28
[0.27]: https://codeberg.org/ralfhersel/youplay/releases/tag/v0.27
